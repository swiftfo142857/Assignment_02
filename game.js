var manager = null;
var emitter = null;
var emitter2 = null;
var emitter3 = null;
var emitter_boss = null;
var generateEnemyTime = 0;
var generateBossTime = 0;
var generateEnemyTimeInterval = 1000;
var sprite;
var weapon;
var cursors;
var fireButton;
var enemy_weapon;
var enemy_weapon2;
var startFire = false;
var startBossFire = false;
var score = 0;
var blood = 100;
var fx;
var boss_fire_rate = 0;
var bufferMode = false;
var bgm;


var startState = {
    preload: function() {
        game.load.image('bg_start','assets/bg.jpg');
        var start_page = document.getElementById('start-page');
        start_page.style.display = "block";
    },
    create: function(){
        this.bg_start = game.add.tileSprite(0,0,game.width, game.height,'bg_start');
        //this.bg_start.setTo(0.8,0.8);
        this.bg_start.autoScroll(0,100);
        var title = document.getElementById('start-title');
        var content = document.getElementById('start-content');
        content.innerText = "Move: up/down/left/right arrow\nAttack: spacebar\nEvery 20 enemies killed, special mode\nRed ten can aid yourself\nThe game will become harder while playing\nBe fun\n"
        title.innerText = "Raiden";
    }
}
var endState = {
    preload: function() {
        game.load.image('bg','assets/bg.jpg');
        var end_page = document.getElementById('end-page');
        var end_page_title = document.getElementById('end-page-title');

        
        end_page.style.display = "block";
        end_page_title.innerText = "You Lose\n Score: "+score;
        bgm.stop();
        
    },
    create: function(){
        this.bg = game.add.tileSprite(0,0,game.width, game.height,'bg');
        this.bg.autoScroll(0,100);
        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.stage.backgroundColor = 0x337799;
    
        emitter = game.add.emitter(0, 0, 100);
    
        emitter.makeParticles('stone');
        emitter.gravity = 200;
        
    
    },
    

}

var mainState = {
    preload: function() {
        game.load.image('bg', 'assets/bg.jpg');
        game.load.image('bullet', 'assets/bullet.png');
        game.load.spritesheet('ship','assets/spaceship5.png',28,21)
        game.load.image('enemy','assets/enemy1.png');
        game.load.image('enemy2','assets/enemy2.png')
        game.load.spritesheet('bullet2', 'assets/rgblaser.png', 4, 4);
        game.load.audio('sfx', 'assets/fx_mixdown.ogg');
        
        game.load.audio('bgm','assets/magical_horror_audiosprite.ogg');

        game.load.image('stone','assets/stone.png');
        game.load.image('muzzleflash3','assets/muzzleflash3.png');
        game.load.image('muzzleflash4','assets/muzzleflash4.png');
        game.load.image('smoke','assets/smoke-puff.png');
        game.load.image('aid','assets/aid.png');
        

        
        
        var start_page = document.getElementById('start-page');
        var end_page = document.getElementById('end-page');
        start_page.style.display = "none";
        end_page.style.display = "none";
        updateBlood(100);
        blood = 100;
        score = 0;
        updateScore();
        generateEnemyTimeInterval = 1000;
        generateEnemyTime = 0;
        bufferMode = false;
        var volumeBar = document.getElementById('volume-bar');
        volumeBar.style.width = 1*100 + '%';
        
    },

    create: function() {

        fx = game.add.audio('sfx');
        fx.allowMultiple = true;
        fx.addMarker('alien death', 1, 1.0);
        fx.addMarker('boss hit', 3, 0.5);
        fx.volume = 1;

        bgm = game.add.audio('bgm');
        bgm.loop = true;
        bgm.volume = 1;
        bgm.play();


        game.physics.startSystem(Phaser.Physics.ARCADE); 

        
        // Background
        this.bg = game.add.tileSprite(0, 0, game.width, game.height, 'bg');
        this.bg.autoScroll(0, 80);

        

        weapon = game.add.weapon(200, 'bullet');
        weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        weapon.bulletAngleOffset = 90;
        weapon.bulletSpeed = 600;
        weapon.fireRate = 100;
        weapon.bulletSpeedVariance = 0;
        game.physics.arcade.enable(weapon);

        sprite = this.add.sprite(320, 500, 'ship');
        game.physics.arcade.enable(sprite);
        sprite.body.collideWorldBounds = true;
        sprite.life = 100;

        sprite.animations.add('right', [3, 4], 16, true);
        /// 4. Create the 'leftjump' animation with frame rate = 16 (frames 7 and 8 and no loop)
        sprite.animations.add('left', [2, 1], 16, true);
        sprite.facingLeft = false;

        //  Tell the Weapon to track the 'player' Sprite, offset by 14px horizontally, 0 vertically
        weapon.trackSprite(sprite, 14, 0);

        cursors = this.input.keyboard.createCursorKeys();

        fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);

        //enemy1
        this.enemys = game.add.group();
        this.enemys.enableBody = true;
        this.enemys.createMultiple(50, 'enemy');
        this.enemys.setAll('outOfBoundsKill', true);
        this.enemys.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.enemys);

        // boss
        this.enemys2 = game.add.group();
        this.enemys2.enableBody = true;
        this.enemys2.createMultiple(5, 'enemy2');
        this.enemys2.setAll('outOfBoundsKill', true);
        this.enemys2.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.enemys2);

        // aid
        this.aids = game.add.group();
        this.aids.enableBody = true;
        this.aids.createMultiple(5, 'aid');
        this.aids.setAll('outOfBoundsKill', true);
        this.aids.setAll('checkWorldBounds', true);
        game.physics.arcade.enable(this.aids);



        //enemy weapon
        enemy_weapon = game.add.weapon(5, 'bullet');
        enemy_weapon.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        enemy_weapon.bulletAngleOffset = 270;
        enemy_weapon.bulletSpeed = -800;
        enemy_weapon.fireRate = 200;
        game.physics.arcade.enable(enemy_weapon);

        // enemy weapon 2
        enemy_weapon2 = game.add.weapon(5, 'stone');
        enemy_weapon2.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
        enemy_weapon2.bulletAngleOffset = 270;
        enemy_weapon2.bulletSpeed = -300;
        enemy_weapon2.fireRate = 200;
        game.physics.arcade.enable(enemy_weapon2);

        game.time.events.loop(Phaser.Timer.SECOND * 10, function(){
            var e = this.aids.getFirstExists(false);
            if(e) {
                e.reset(game.rnd.integerInRange(10, 350), 0);
                e.body.velocity.y = 100;
            }
        }, this);

        // particle system
        game.stage.backgroundColor = 0x337799;
        emitter2 = game.add.emitter(50, 50, 100);
        emitter2.makeParticles('muzzleflash4');
        emitter2.gravity = 200;
        emitter_boss = game.add.emitter(50, 50, 100);
        emitter_boss.makeParticles('muzzleflash3');
        emitter_boss.gravity = 200;
        emitter3 = game.add.emitter(50, 50, 100);
        emitter3.makeParticles('smoke');
        emitter3.gravity = 200;

    },
    createBigEnemy(){
        console.log("create big")
        startFire = true;
        var e = this.enemys2.getFirstExists(false);
        if(e) {
            enemy_weapon2.trackSprite(e, 50, 150);
            e.reset(game.rnd.integerInRange(0, 350), 0);
            e.life = 10;
            e.body.velocity.y = 30;
        }
    },

    /// Call moving function in each frame.
    update: function() {
        this.movePlayer();
        this.generateEnemy();
        this.generateBoss();
        if(startFire){
            enemy_weapon.fire();
            game.physics.arcade.overlap(weapon.bullets, this.enemys, this.hitEnemy, null, this);
            game.physics.arcade.overlap(enemy_weapon.bullets, sprite, this.bulletHitMe, null, this);
            game.physics.arcade.overlap(this.enemys, sprite, this.enemyHitMe, null, this);
            game.physics.arcade.overlap(weapon.bullets, this.enemys2, this.hitBoss, null, this);
            game.physics.arcade.overlap(enemy_weapon2.bullets, sprite, this.bulletHitMe, null, this);
            game.physics.arcade.overlap(this.enemys2, sprite, this.enemyHitMe, null, this);
            game.physics.arcade.overlap(this.aids, sprite, this.hitAid, null, this);
        }
        if(startBossFire){
            if(game.time.now > boss_fire_rate){
                enemy_weapon2.fire();
                boss_fire_rate = game.time.now + 1000
            }
                
        }
        if(score % 20 == 0 && score !=0){
            bufferMode = true;
            setInterval(function(){
                bufferMode = false;
            },150000)
        }
            
        
    },
    hitAid(aid,player){
        blood = 100;
        updateBlood(blood);
    },
    generateBoss(){
        if(game.time.now > (generateBossTime || 0)) {
            startBossFire = true;
            var e = this.enemys2.getFirstExists(false);
            if(e) {
                enemy_weapon2.trackSprite(e, 60, 120);
                e.reset(game.rnd.integerInRange(0, 350), 0);
                e.life = 10;
                e.body.velocity.y = 50;
            }
            generateBossTime = game.time.now + 10000;
            
        }
    },
    generateEnemy(){
        if(game.time.now > (generateEnemyTime || 0)) {
            startFire = true;
            var e = this.enemys.getFirstExists(false);
            if(e) {
                enemy_weapon.trackSprite(e, 25, 60);
                e.reset(game.rnd.integerInRange(0, 350), 0);
                e.life = 2;
                e.body.velocity.y = 200;
            }
            generateEnemyTime = game.time.now + generateEnemyTimeInterval;
            generateEnemyTimeInterval = generateEnemyTimeInterval - 5;

        }
    },
    hitBoss(bullet,enemy){
        bullet.kill();
        fx.play('alien death');
        enemy.life--;
        if(enemy.life <= 0) {
            //console.log(enemy)
            emitter_boss.x = enemy.x+20;
            emitter_boss.y = enemy.y+20;
            emitter_boss.start(true, 2000, null, 10);
            //console.log(this.emitter)
            enemy.kill();
            startBossFire = false;
            score= score+5;
            updateScore();
        }
    },
    hitEnemy(bullet, enemy){
        // bullet hit enemy;
        bullet.kill();
        fx.play('alien death');
        enemy.life--;
        if(enemy.life <= 0) {
            //console.log(enemy)
            emitter2.x = enemy.x;
            emitter2.y = enemy.y;
            emitter2.start(true, 2000, null, 10);
            //console.log(this.emitter)
            enemy.kill();
            startFire = false;
            score++;
            updateScore();
        }

    },
    enemyHitMe(bullet,sprite){
        emitter3.x = sprite.x;
        emitter3.y = sprite.y;
        emitter3.start(true, 2000, null, 10);
        updateBlood(blood-=5);
    },
    bulletHitMe(enemy,sprite){
        emitter3.x = sprite.x;
        emitter3.y = sprite.y;
        emitter3.start(true, 2000, null, 10);
        updateBlood(blood-=2);
    },
    createEnemy(){
        startFire = true;
        var e = this.enemys.getFirstExists(false);
        if(e) {
            enemy_weapon.trackSprite(e, 25, 60);
            e.reset(game.rnd.integerInRange(0, 350), 0);
            e.life = 10;
            e.body.velocity.y = -500;
        }
    },
    movePlayer: function(){
        sprite.body.velocity.x = 0;
        sprite.body.velocity.y = 0;
        if (cursors.left.isDown){
            sprite.body.velocity.x = -200;
            sprite.animations.play('left');
        }
        else if (cursors.right.isDown){
            sprite.body.velocity.x = 200;
            sprite.animations.play('right');
        }
        else if (cursors.up.isDown){
            sprite.body.velocity.y = -200;
            sprite.animations.stop();
        }
        else if (cursors.down.isDown){
            sprite.body.velocity.y = 200;
            sprite.animations.stop();
        }
        else{
            sprite.animations.stop();
            sprite.frame = 0;
        }
        if (fireButton.isDown)
        {
            if(bufferMode){
                var bullet = weapon.bullets.getFirstExists(false);
                if(bullet) {
                    if(game.time.now > (sprite.bulletTime || 0)) {
                        bullet.reset(sprite.x + 10, sprite.y + 10);
                        bullet.body.velocity.y = -400;
                        bullet.body.velocity.x = -100;
                        //sprite.bulletTime = game.time.now + 5;
                        //console.log("fire1");
                        weapon.fire();
                    }
                }
                bullet = weapon.bullets.getFirstExists(false);
                if(bullet) {
                    if(game.time.now > (sprite.bulletTime || 0)) {
                        bullet.reset(sprite.x + 10, sprite.y + 10);
                        bullet.body.velocity.y = -400;
                        //sprite.bulletTime = game.time.now + 5;
                        //console.log("fire2");
                        weapon.fire();
                    }
                }
                bullet = weapon.bullets.getFirstExists(false);
                if(bullet) {
                    if(game.time.now > (sprite.bulletTime || 0)) {
                        bullet.reset(sprite.x + 10, sprite.y + 10);
                        bullet.body.velocity.y = -400;
                        bullet.body.velocity.x = 100;
                        sprite.bulletTime = game.time.now + 100;
                        //console.log("fire3");
                        weapon.fire();
                    }
                }
            }
            else{
                weapon.fire();
            }
                
            
            
        }
    },
    render: function() {
        
    }
    

};


/// ToDo: 1. Initialize Phaser (width: 500, height: 340) 
///		  2. Add our state(mainState)
///		  3. Start it!!

var game = new Phaser.Game(400, 550, Phaser.AUTO, "canvas");
game.state.add('start',startState);
//game.state.start('startGame')
game.state.add('main', mainState);
game.state.add('end',endState);
game.state.start('start'); 

function goToState(state){
    game.state.start(state); 
}
function runCommand(command){
    console.log(command)
    if(command == "start"){
        game.paused = false;
    }
    else if (command == "stop"){
        game.paused = true;
    }
    else if (command == "volumeUp"){
        fx.volume+=0.1;
        bgm.volume+=0.1;
        if(bgm.volume>1)    bgm.volume = 1;
        if(fx.volume>1)    fx.volume = 1;
        //console.log(fx.volume);
        var volumeBar = document.getElementById('volume-bar');
        volumeBar.style.width = bgm.volume*100 + '%';
        
    }
    else if (command == "volumeDown"){
        fx.volume-=0.1;
        bgm.volume-=0.1;
        if(bgm.volume<0)    bgm.volume = 0;
        if(fx.volume<0)    fx.volume = 0;
        //console.log(fx.volume);
        var volumeBar = document.getElementById('volume-bar');
        volumeBar.style.width = bgm.volume*100 + '%';
    }
}
function updateScore(){
    var _score = document.getElementById('score');
    _score.innerText = "Score: "+ score
}
function updateBlood(blood) {
    var elem = document.getElementById("myBar");
    if (blood < 0) {
        goToState('end');
    }
    else {
        elem.style.width = blood + '%';
        elem.innerText = blood * 1 + '%';
        elem.style.backgroundColor = 'green';
    }
    if(blood <=50) {
        elem.style.backgroundColor = 'red';
    }
} 

function getLeaderBoard(){
    var boardRef = firebase.database().ref('board');
    var scores = [];
    boardRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot) {
                scores.push(childSnapshot.val());
            })
        })
        .then(()=>{
            scores = scores.sort(function(a,b){
                return a.score < b.score?1:-1;
            })
        })
        .then(()=>{
            var leaderBoardPage = document.getElementById("leaderboard-page")
            var leaderBoardPageContent = document.getElementById("leaderboard-page-content")
            leaderBoardPage.style.display = "block";
            document.getElementById('end-page').style.display = "none";
            var string = "1st: "+scores[0].name+" "+scores[0].score+
            "\n2nd: "+scores[1].name+" "+scores[1].score+
            "\n3rd: "+scores[2].name+" "+scores[2].score+
            "\n4rd: "+scores[3].name+" "+scores[3].score+
            "\n5rd: "+scores[4].name+" "+scores[4].score
            console.log(string)
            leaderBoardPageContent.innerText = string
        })

    
}

function submitScore(){
    var nickname = document.getElementById('nickname');
    var boardRef = firebase.database().ref('board');
    var newPostRef = boardRef.push();
    newPostRef
    .set({
        "name":nickname.value,
        "score":score
    })
    .then(()=>{
        nickname.value = "";
        console.log("hi")
    })
    
}

function backToMenu(){
    document.getElementById('end-page').style.display = "block";
    document.getElementById('leaderboard-page').style.display = "none";
}