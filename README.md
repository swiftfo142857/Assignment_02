# Software Studio 2019 Spring Assignment 2

106062209 黎彥君

https://swiftfo142857.gitlab.io/Assignment_02

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : 
Level : The time interval to generate enemy will decrease while playing the game
Skill : You can use ultimate skills whenever you kills 20 enemies
2. Animations : 
The spaceship have an animation left/right
3. Particle Systems : 
When the spaceship is attacked, there's a smoke particle system.
When enemy is died, there's a blow-up particle system.
4. Sound effects : 
There are background bgm and special sound when you hits enemy.
5. Leaderboard : 
The leader board connected to firebase and it can get  5 highest score and player's name.
When you finish the game, you can type in your name and save your game record to our firebase database. Then you can click the `leaderboard` button to see the leader board.
6. UI
Play, Pause Game, Volume controller, blood bar (total: 100)

* When you hit by other plane, you probably die immediately.

# Bonus Functions Description : 
1. Boss : 
Boss use rocks to attack you, it moves more slowly and its life is 5 times more than normal enemy.
2. Aid : 
There are some aids you can take routinely, you can recover his/her life to 100 when you stays on it.

